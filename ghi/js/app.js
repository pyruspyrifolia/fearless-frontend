
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts);
            const ends = new Date(details.conference.ends);
            const startDate = starts.toDateString();
            const endsDate = ends.toDateString();
            const locationName = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endsDate, locationName);
            const column =document.querySelector('.row');
            column.innerHTML += html
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e)
    }

  });

  function createCard(title, description, pictureUrl, startDate, endsDate, locationName) {
    return `
    <div class="col-4">
      <div class="card shadow-sm mb-2">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">Starts: ${startDate} - Ends: ${endsDate}</div>
      </div>
    `;
  }
