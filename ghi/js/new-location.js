//  You quickly confer and agree that the code needed when the page loads will call that RESTful API you just created,
//get the data back, then loop through it. And for each state in it,
// it'll create an option element that has a value of the abbreviation and the text of the name.

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/";

    const response = await fetch(url);
    const selectTag = document.getElementById("state")

    if (response.ok) {
        const data = await response.json();
        console.log(data)
        for (let state of data.States) {
        const option = document.createElement("option");
        option.value = state.Abbreviation;
        option.innerHTML = state.Name;
        selectTag.appendChild(option);
    }
}



const formTag = document.getElementById('create-location-form');
formTag.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json)
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
    'Content-Type': 'application/json',
    },
};
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
    formTag.reset();
    const newLocation = await response.json();
}
    });
});
